+++
title = "Sentiment Analysis Model for Product Reviews"
[extra]
image = "../sentiment.jpg"
link = "https://github.com/jiwonny29/NLP-Sentiment-Analysis-Models-Comparison"
technologies = ["NLP,", "LSTM", "Python"]
+++
Developed and compared sentiment analysis models for online product reviews, employing a Generative Language Model (Multinomial Naive Bayes) and a Discriminative Neural Network (Bidirectional LSTM).