+++
title = "Skills"
template = "skills.html"
page_template = "skills.html"

[extra]
author = ""
image = "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ad/Placeholder_no_text.svg/1200px-Placeholder_no_text.svg.png"

lan = [
{ lang = "Python", expr = "5", image = "../python2.png", link = "https://www.python.org/"}, 
{ lang = "R", expr = "3", image = "../R.png", link = "https://www.r-project.org/"}, 
{ lang = "SQL", expr = "3", image = "../sql.png", link = "https://www.mysql.com/"},
{ lang = "Rust", expr = "2", image = "../rust.png", link = "https://www.rust-lang.org//"}, 
]

tools = [
{ tool = "AWS", expr = "4", image = "../aws.png", link = "https://aws.amazon.com/"},
{ tool = "GitHub", expr = "4", image = "../github.png", link = "https://github.com/"},
{ tool = "Azure Databricks", expr = "3", image = "../databricks.png", link = "https://www.databricks.com/product/azure"},
{ tool = "GitLab", expr = "3", image = "../gitlab.png", link = "https://gitlab.com/"},
]
+++
