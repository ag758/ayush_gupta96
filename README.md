# Static Webpage with Zola - Mini Project1

This repository contains a static webpage created using Zola, a fast static site generator. The webpage is hosted on GitLab Pages.

Link to Webpage : https://ayush-gupta96-ag758-dd3c570f98f861f58ac2b749955ccef9c9b218cbf16.gitlab.io/

## Getting Started

To view the webpage locally or make modifications, follow these steps:

1. **Clone the Repository**: 
   ```
   git clone <repository_url>
   ```

2. **Install Zola**: 
   Follow the [official installation instructions](https://www.getzola.org/documentation/getting-started/installation/) to install Zola on your system.

3. **Navigate to the Project Directory**: 
   ```
   cd <project_directory>
   ```

4. **Build the Site**: 
   ```
   zola build
   ```

5. **View the Site Locally**: 
   After building the site, you can view it locally by running:
   ```
   zola serve
   ```
   Open your web browser and go to `http://127.0.0.1:1111`.

## Contributing

If you'd like to contribute to this project, you can do so by following these steps:

1. Fork this repository.

2. Create a new branch:
   ```
   git checkout -b feature/new-feature
   ```

3. Make your changes and commit them:
   ```
   git commit -am 'Add new feature'
   ```

4. Push to the branch:
   ```
   git push origin feature/new-feature
   ```

5. Create a new Pull Request.

## Deployment

This webpage is automatically deployed using GitLab Pages. Upon pushing changes to the `main` branch, the webpage is rebuilt and updated.

## Resources

- [Zola Documentation](https://www.getzola.org/documentation/getting-started/installation/)
- [GitLab Pages Documentation](https://docs.gitlab.com/ee/user/project/pages/)

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
